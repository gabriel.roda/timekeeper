"use strict";

var hora = getCookie("hourTime");
var minutos = getCookie("minutesTime");

if (hora.length > 0) {
	document.querySelector(".horario .horas").value = hora;
	setHours(hora);
}

if (minutos.length > 0) {
	document.querySelector(".horario .minutos").value = minutos;
	setMinutes(minutos);
}

let gradientStart = getCookie("gradient-start");
let gradientStop = getCookie("gradient-stop");

document.addEventListener("load", () => {
	if (gradientStart != "") {
		root.style.setProperty("--gradient-start", gradientStart);
	}
	if (gradientStop != "") {
		root.style.setProperty("--gradient-stop", gradientStop);
	}
});

/*----- -----*/

var agora = new Date();
var pBars = [];
var atualizador;

var ateExp = new Date();
ateExp.setHours(17);
ateExp.setMinutes(55);
ateExp.setSeconds(0);

let feriados;
let pagamentos;

let ateFeriado = new Date();
let atePagamento = new Date();
let ateFerias = new Date();

let saveStartColor;
let saveStopColor;

//carregar pagamentos, feriados e ferias do firestore
fs.collection("uniasselvi")
	.doc("datas")
	.get()
	.then((doc) => {
		feriados = doc.data().feriados;
		pagamentos = doc.data().pagamentos;

		ateFerias = doc.data().ferias.toDate();

		//encontra o próximo feriado
		for (let indexFer in feriados) {
			//tranformo a timestamp em Date
			let itemFer = feriados[indexFer].toDate();

			if (itemFer > ateFeriado) {
				ateFeriado = itemFer;
				break;
			}
		}

		//encontra o próximo pagamento
		for (let indexPag in pagamentos) {
			let itemPag = pagamentos[indexPag].toDate();

			if (itemPag > atePagamento) {
				atePagamento = itemPag;
				break;
			}
		}

		atualizador = window.setInterval(
			fill,
			1000,
			ateFerias,
			ateFeriado,
			atePagamento
		);
	});

const msIna = {
	second: 1000,
	minute: 1000 * 60,
	hour: 1000 * 60 * 60,
	day: 1000 * 60 * 60 * 24,
};

function intervalBreakdown(howLong) {
	var tempDate = new Date(howLong);

	var dias = parseInt(tempDate / msIna.day);
	tempDate = dias > 0 ? tempDate % (dias * msIna.day) : tempDate;

	var horas = parseInt(tempDate / msIna.hour);
	tempDate = horas > 0 ? tempDate % (horas * msIna.hour) : tempDate;

	var minutos = parseInt(tempDate / msIna.minute);
	tempDate = minutos > 0 ? tempDate % (minutos * msIna.minute) : tempDate;

	var segundos = parseInt(tempDate / 1000);

	return {
		dias: dias,
		horas: horas,
		minutos: minutos,
		segundos: segundos,
	};
}

document.querySelectorAll(".progress-bar").forEach((el) => {
	pBars.push(
		new ProgressBar.Line(el, {
			strokeWidth: 20,
			easing: "easeInOut",
			duration: 1400,
			color: "#ff6347",
			trailWidth: 20,
			svgStyle: {
				width: "100%",
				height: "100%",
			},
			from: {
				color: "#ff2700",
			},
			to: {
				color: "#00e200",
			},
			step: (state, bar) => {
				bar.path.setAttribute("stroke", state.color);
			},
		})
	);
});

function fill(ateFerias, ateFeriado, atePagamento) {
	//pega os campos que tem os timings
	var els = document.querySelectorAll(".panel-main");

	//preenche eles na ordem, comecando por expediente
	var msgString = "";
	var expediente = intervalBreakdown(ateExp - new Date());

	if (expediente.segundos < 0) {
		msgString += "0 segundos";
	} else {
		if (expediente.dias > 0) msgString += expediente.dias + " dias, ";
		if (expediente.horas > 0) msgString += expediente.horas + " horas, ";
		if (expediente.minutos > 0) msgString += expediente.minutos + " minutos e ";
		msgString += expediente.segundos + " segundos";
	}
	els[0].innerHTML = msgString;

	//depois pagamento
	msgString = "";
	var pagamento = intervalBreakdown(atePagamento - new Date());

	if (pagamento.dias > 0) msgString += pagamento.dias + " dias, ";
	if (pagamento.horas > 0) msgString += pagamento.horas + " horas, ";
	if (pagamento.minutos > 0) msgString += pagamento.minutos + " minutos e ";
	msgString += pagamento.segundos + " segundos";

	els[1].innerHTML = msgString;

	//proximo feriado
	msgString = "";
	var feriado = intervalBreakdown(ateFeriado - new Date());

	if (feriado.dias > 0) msgString += feriado.dias + " dias, ";
	if (feriado.horas > 0) msgString += feriado.horas + " horas, ";
	if (feriado.minutos > 0) msgString += feriado.minutos + " minutos e ";
	msgString += feriado.segundos + " segundos";

	els[2].innerHTML = msgString;

	//ferias de fim de ano
	msgString = "";
	var ferias = intervalBreakdown(ateFerias - new Date());

	if (ferias.dias > 0) msgString += ferias.dias + " dias, ";
	if (ferias.horas > 0) msgString += ferias.horas + " horas, ";
	if (ferias.minutos > 0) msgString += ferias.minutos + " minutos e ";
	msgString += ferias.segundos + " segundos";

	els[3].innerHTML = msgString;

	//--barras de progesso
	pBars[0].animate(dayPercentage(new Date()));
	pBars[1].animate(targetPercentage(pagamentos));
	pBars[2].animate(targetPercentage(feriados));
	pBars[3].animate(vacationPercentage(new Date()));
}

function dayPercentage(actualTime) {
	let endTime = new Date();
	endTime.setHours(document.querySelector(".horario .horas").value);
	endTime.setMinutes(document.querySelector(".horario .minutos").value);
	endTime.setSeconds(0);

	let workJourney = 36000000;

	let dif = endTime - actualTime;

	let percent = (1 - dif / workJourney).toFixed(2);
	// console.log(percent);

	return percent;
}

function targetPercentage(datesArray) {
	let lastOccurence;
	let nextOccurence;

	let present = new Date();

	for (let i = 0; i < datesArray.length; i++) {
		let dateItem = datesArray[i].toDate();

		if (dateItem > present) {
			try {
				lastOccurence = datesArray[i - 1].toDate();
			} catch (e) {
				lastOccurence = new Date();
			}
			nextOccurence = dateItem;
			break;
		}
	}

	let timeframe = nextOccurence - lastOccurence;
	let currentPoint = present - lastOccurence;

	return (currentPoint / timeframe).toFixed(2);
}

function vacationPercentage(actualTime) {
	let startTime = new Date("2021-01-01T00:00:00");

	let workJourney = 2.989e10;

	let dif = actualTime - startTime;

	let percent = (dif / workJourney).toFixed(2);

	return percent;
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(";");
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == " ") {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function setHours(value) {
	// var value = document.getElementById('config-hora').value;

	ateExp.setHours(value);
	ateFeriado.setHours(value - 1);
	ateFerias.setHours(value);

	setCookie("hourTime", value);
}

function setMinutes(value) {
	// var value = document.getElementById('config-minutos').value;

	ateExp.setMinutes(value);
	ateFeriado.setMinutes(value);
	ateFerias.setMinutes(value);

	setCookie("minutesTime", value);
}

function toggleFs() {
	var elem = document.body;

	if (window.parent.screen.height - 50 > window.parent.innerHeight) {
		if (elem.requestFullscreen) {
			elem.requestFullscreen();
		} else if (elem.mozRequestFullScreen) {
			/* Firefox */
			elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullscreen) {
			/* Chrome, Safari & Opera */
			elem.webkitRequestFullscreen();
		} else if (elem.msRequestFullscreen) {
			elem.msRequestFullscreen(); /* IE/Edge */
		}
	} else {
		try {
			var cpDoc = window.parent.document;

			if (cpDoc.exitFullscreen) {
				cpDoc.exitFullscreen();
			} else if (cpDoc.webkitExitFullscreen) {
				cpDoc.webkitExitFullscreen();
			} else if (cpDoc.mozCancelFullScreen) {
				cpDoc.mozCancelFullScreen();
			} else if (cpDoc.msExitFullscreen) {
				cpDoc.msExitFullscreen();
			}
		} catch (e) {
			console.log("Error getting out of fullscreen");
			console.log(e);
		}
	}
}

function toggleConfig() {
	document.querySelector(".config-pannel").classList.toggle("active");
}

// color pickr
let pickerOptions = {
	el: ".color-button.start",
	container: "body",
	theme: "monolith",
	closeOnScroll: true,
	useAsButton: true,
	outputPrecision: 0,
	defaultRepresentation: "HEXA",
	components: {
		palette: false,
		preview: true,
		hue: true,
		opacity: true,
		interaction: {
			hex: true,
			rgba: true,
			input: true,
			cancel: true,
			save: true,
		},
	},
};

let root = document.documentElement;
let startG;
let startPicker;

let stopG;
let stopPicker;

document.querySelector(".color-button.start").addEventListener("click", () => {
	startG = getComputedStyle(document.documentElement).getPropertyValue(
		"--gradient-start"
	);
	pickerOptions.default = startG.trim();
	pickerOptions.el = ".color-button.start";

	//reseta o picker
	if (startPicker) {
		startPicker.destroyAndRemove();
	}

	//cria uma nova instancia
	startPicker = new Pickr(pickerOptions);
	startPicker.show();

	//listeners
	startPicker.on("change", (color, instance) => {
		let colorHex = color.toHEXA().toString();
		root.style.setProperty("--gradient-start", colorHex);
		setCookie("gradient-start", colorHex, 100);
	});

	startPicker.on("save", (color, instance) => {
		root.style.setProperty("--gradient-start", color.toHEXA().toString());
		instance.hide();
	});

	startPicker.on("cancel", (instance) => {
		root.style.setProperty("--gradient-start", instance.options.default);
		setCookie("gradient-start", instance.options.default, 100);
		instance.hide();
	});
});

document.querySelector(".color-button.stop").addEventListener("click", () => {
	stopG = getComputedStyle(document.documentElement).getPropertyValue(
		"--gradient-stop"
	);
	pickerOptions.default = stopG.trim();
	pickerOptions.el = ".color-button.stop";

	//reseta o picker
	if (stopPicker) {
		stopPicker.destroyAndRemove();
	}

	//cria uma nova instancia
	stopPicker = new Pickr(pickerOptions);
	stopPicker.show();

	//listeners
	stopPicker.on("change", (color, instance) => {
		let colorHex = color.toHEXA().toString();
		root.style.setProperty("--gradient-stop", colorHex);
		setCookie("gradient-stop", colorHex, 100);
	});

	stopPicker.on("save", (color, instance) => {
		root.style.setProperty("--gradient-stop", color.toHEXA().toString());
		instance.hide();
	});

	stopPicker.on("cancel", (instance) => {
		root.style.setProperty("--gradient-stop", instance.options.default);
		setCookie("gradient-stop", instance.options.default, 100);
		instance.hide();
	});
});
