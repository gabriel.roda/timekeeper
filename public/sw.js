'use strict';

importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

self.addEventListener('install', function (evt) {
  self.skipWaiting();
});

self.addEventListener('activate', function (evt) {
  self.clients.claim();
});

workbox.routing.registerRoute(
// Cache CSS files.
/\.css$/,
// Use cache but update in the background.
new workbox.strategies.StaleWhileRevalidate({
  // Use a custom cache name.
  cacheName: 'css-cache'
}));

workbox.routing.registerRoute(
// Cache image files.
/\.(?:png|jpg|jpeg|svg|gif)$/,
// Use the cache if it's available.
new workbox.strategies.CacheFirst({
  // Use a custom cache name.
  cacheName: 'image-cache',
  plugins: [new workbox.expiration.Plugin({
    // Cache only 20 images.
    maxEntries: 20,
    // Cache for a maximum of a week.
    maxAgeSeconds: 7 * 24 * 60 * 60
  })]
}));

workbox.precaching.precacheAndRoute([
  {
    "url": "404.html",
    "revision": "0a27a4163254fc8fce870c8cc3a3f94f"
  },
  {
    "url": "img/calendario.png",
    "revision": "0fe9280490ad7be89155b9bdd97ca7f3"
  },
  {
    "url": "img/calendario.svg",
    "revision": "ba0b0714e3c60d280cdc112ba14f4a6b"
  },
  {
    "url": "img/favicon.png",
    "revision": "4a28d3ca7f0c2a0a562a58987785aa26"
  },
  {
    "url": "img/i-128.png",
    "revision": "da7e0db030967f2f5ccb1d4d70251760"
  },
  {
    "url": "img/i-144.png",
    "revision": "6c28d59070f76c32fc1ab6ce59cbab73"
  },
  {
    "url": "img/i-152.png",
    "revision": "75df6529a2a572ffda081478654a6ce8"
  },
  {
    "url": "img/i-192.png",
    "revision": "a3a812c9d342ba8ba42f83ac421c855b"
  },
  {
    "url": "img/i-256.png",
    "revision": "94dcb998ca655f807cca452a223815d2"
  },
  {
    "url": "img/i-512.png",
    "revision": "9fd0c02608b0c1bf337ce551483ef1e7"
  },
  {
    "url": "index.html",
    "revision": "8f946c02df71d68609f2b95147fef9ef"
  },
  {
    "url": "manifest.json",
    "revision": "af78d4e3489abfec44e8a7512961324f"
  },
  {
    "url": "service-worker-pre.js",
    "revision": "2bfb2f84d73fcda773193dc8e47ca47f"
  },
  {
    "url": "src/data.js",
    "revision": "3077baf3c864365fb510153037c447a5"
  },
  {
    "url": "src/main.js",
    "revision": "e4a4ace06186ce6eefe366896902d98d"
  },
  {
    "url": "src/normalize.css",
    "revision": "139411c18180c4889028fe6719d9da05"
  },
  {
    "url": "src/progressbar.min.js",
    "revision": "eca176eff7c99e6435807c1447e56fab"
  },
  {
    "url": "src/react_components.js",
    "revision": "81904a248689720a800f582d669805e9"
  },
  {
    "url": "src/style.css",
    "revision": "e61de05d0762a4f47f2b694bbb2b0863"
  }
]);

importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

workbox.googleAnalytics.initialize();