module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{svg,png,html,json,js,woff,css}"
  ],
  "swDest": "public\\sw.js",
  "swSrc": "./public/service-worker-pre.js"
};